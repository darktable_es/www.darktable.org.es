---
layout: page
title: Inicio
---

![logo darktable](img/logo_dt-es_grande_con_fondo.png)

## Bienvenido a la comunidad de darktable en español.

Bienvenido al lugar de encuentro para todos los usuarios darktable hispanoparlantes. Aquí nos resolvemos dudas, hacemos recomendaciones, aprendemos cosas nuevas y discutimos sobre nuestras mejores obras. Aquí encontrarás un bar de amigos, una galería de arte, una escuela de fotografía, un taller técnico y mucho más.

Esta página es sólo informativa. Toda la *vidilla* está en el foro en el siguiente enlace:

# [FORO DARKTABLE](https://foro.darktable.org.es)
