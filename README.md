![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

---

darktable.org.es es la comunidad de usuarios darktable en español.
Su mayor actividad se centra en el foro [https://foro.darktable.org.es](https://foro.darktable.org.es) 

Este repositorio es el código de la web [https://www.darktable.org.es](https://www.darktable.org.es)

Si usas darktable o te gusta la fotografía con herramientas libres en general, este es tu sitio.

---
